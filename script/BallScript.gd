extends RigidBody

var STATE = preload("states.gd")

export var power = 10
export var view_sensitivity = 0.3
export var air_density = 0.2

var distance = 0.0
var radius = 0.4
var current_state = STATE.EXPLORE
var yaw = 0
var pitch = 0
var curr_power = 0
var prev_time = 0
var ball_yaw = null
var ball_pitch = null

var self_transform = null
var ball_yaw_transform = null
var ball_pitch_transform = null
var ball_indicator = null
var ball_camera = null
var power_indicator = null

func _ready():
	add_to_group("game_entity")
	# Do not collide with free camera
	add_collision_exception_with(get_node("../CameraBody"))
	distance = 0.0
	self_transform = get_transform()
	# Yaw and pitch nodes
	ball_yaw = get_node("BallYaw")
	ball_pitch = ball_yaw.get_node("BallPitch")
	ball_yaw_transform = ball_yaw.get_transform()
	ball_pitch_transform = ball_pitch.get_transform()
	# Indicator
	ball_indicator = ball_pitch.get_node("DirectionIndicator")
	# Camera
	ball_camera = ball_pitch.get_node("BallCamera")
	# Power indicator (GUI)
	power_indicator = get_node("../GUIs/PreGameGUI/PowerIndicator")

func update_slider(dt):
	prev_time += (dt * 4)
	if prev_time > PI / 2:
		prev_time = -PI / 2
	curr_power = power - cos(prev_time) * power
	power_indicator.set_value(1 - curr_power / power)

func is_state(state):
	return current_state == state

func game_restart():
	ball_yaw.set_transform(ball_yaw_transform)
	ball_pitch.set_transform(ball_pitch_transform)
	set_transform(self_transform)
	ball_indicator.show()
	yaw = 0
	pitch = 0
	distance = 0
	curr_power = 0
	prev_time = 0

func state_changed(state):
	current_state = state
	if state == STATE.ORIENT or state == STATE.LAUNCH:
		ball_camera.make_current()
	elif state == STATE.PLAY:
		var dir_vec = (get_global_transform().origin - ball_indicator.get_global_transform().origin).normalized()
		apply_impulse(Vector3(0,0,0), dir_vec * curr_power)
		ball_indicator.hide()
		set_sleeping(false)
	current_state = state

func _integrate_forces(state):
	var vel = state.get_linear_velocity()
	var diff = vel.length() - (air_density * state.get_step())
	var new_vel
	if diff > 0:
		set_linear_velocity(vel.normalized() * diff)
	state.integrate_forces()

func parse_input(ev):
	if ev.type == InputEvent.MOUSE_MOTION:
		yaw = fmod(yaw - ev.relative_x * view_sensitivity, 360)
		pitch = max(min(pitch - ev.relative_y * view_sensitivity, 90), -90)
		ball_yaw.set_rotation(Vector3(0, deg2rad(yaw), 0))
		ball_pitch.set_rotation(Vector3(deg2rad(pitch), 0, 0))

func get_distance():
	return distance

func add_distance(delta):
	distance += get_linear_velocity().length() * delta