
extends WorldEnvironment

var STATE = preload("states.gd")

var current_state = STATE.EXPLORE
var state_can_change = true
var state_timer = null
var ball_timer = null
var ball_timer_started = false
var game_won = false

func _ready():
	state_timer = get_node("StateTimer")
	ball_timer = get_node("BallTimer")
	set_fixed_process(true)
	set_process_input(true)
	add_to_group("game")
	if OS.has_touchscreen_ui_hint():
		get_ui_touch().show()
	set_state(STATE.EXPLORE)

func _input(ev):
	if ev.type == InputEvent.MOUSE_BUTTON:
		if not OS.has_touchscreen_ui_hint():
			if is_state(STATE.ORIENT) and ev.is_pressed():
				set_state(STATE.LAUNCH)
			elif is_state(STATE.LAUNCH) and not ev.is_pressed():
				set_state(STATE.PLAY)
	if ev.type == InputEvent.MOUSE_MOTION:
		if OS.has_touchscreen_ui_hint() and ev.y > get_ui_touch().get_pos().y:
			pass
		else:
			if is_state(STATE.ORIENT) or is_state(STATE.LAUNCH):
				get_ball_body().parse_input(ev)
			elif is_state(STATE.EXPLORE) or is_state(STATE.PLAY):
				get_camera_body().parse_input(ev)

func _fixed_process(delta):
	if is_state(STATE.PLAY):
		# Move free camera in play mode
		get_camera_body().parse_keyboard(delta)
		# Do the math in play state
		get_ball_body().add_distance(delta)
		update_in_game_text()
		is_in_hole()
		if get_ball_body().get_linear_velocity().length() < 0.2:
			if not ball_timer_started:
				ball_timer_started = true
				ball_timer.start()
		else:
			ball_timer.stop()
			ball_timer_started = false
	else:
		# Stop the ball in other states
		if not get_ball_body().is_sleeping():
			get_ball_body().set_sleeping(true)
		# Update slider in launch state
		if is_state(STATE.LAUNCH):
			get_ball_body().update_slider(delta)
			if OS.has_touchscreen_ui_hint() and not Input.is_action_pressed("launch_ball"):
				set_state(STATE.PLAY)
		# Parse camera keyboard in free mode
		elif is_state(STATE.EXPLORE):
			get_camera_body().parse_keyboard(delta)
		# Lunch button in touch mode
		elif is_state(STATE.ORIENT) and OS.has_touchscreen_ui_hint():
			if Input.is_action_pressed("launch_ball"):
				set_state(STATE.LAUNCH)
	# Manage spacebar for state change EXPLORE <-> ORIENT
	if Input.is_action_pressed("state_switch"):
		if is_state(STATE.EXPLORE):
			set_state(STATE.ORIENT)
		elif is_state(STATE.ORIENT):
			set_state(STATE.EXPLORE)

func is_state(state):
	return current_state == state

func set_state(state):
	if state_can_change:
		state_can_change = false
		current_state = state
		if not is_state(STATE.END):
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		if is_state(STATE.EXPLORE):
			get_in_game_gui().hide()
			get_end_game_gui().hide()
			get_hold_mouse().hide()
			get_release_mouse().hide()
			get_power_indicator().hide()
			get_pre_game_gui().show()
		if is_state(STATE.ORIENT):
			get_in_game_gui().hide()
			get_end_game_gui().hide()
			get_release_mouse().hide()
			get_power_indicator().hide()
			get_pre_game_gui().show()
			get_hold_mouse().show()
		if is_state(STATE.LAUNCH):
			get_in_game_gui().hide()
			get_end_game_gui().hide()
			get_hold_mouse().hide()
			get_pre_game_gui().show()
			get_release_mouse().show()
			get_power_indicator().show()
		if is_state(STATE.PLAY):
			get_pre_game_gui().hide()
			get_end_game_gui().hide()
			get_in_game_gui().show()
		if is_state(STATE.END):
			get_pre_game_gui().hide()
			get_in_game_gui().hide()
			get_end_game_gui().show()
		get_tree().call_group(0, "game_entity", "state_changed", current_state)
		state_timer.start()

func is_in_hole():
	if (get_ball_body().get_global_transform().origin - get_hole().get_global_transform().origin).length() < 1:
		ball_timer.stop()
		game_won = true
		display_end_game()
		set_state(STATE.END)

func restore_mouse():
	if current_state != STATE.END:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

var __ball_body = null
func get_ball_body():
	if __ball_body == null:
		__ball_body = get_node("GameBall")
	return __ball_body

var __camera_body = null
func get_camera_body():
	if __camera_body == null:
		__camera_body = get_node("CameraBody")
	return __camera_body

var __hole = null
func get_hole():
	if __hole == null:
		__hole = get_node("Hole")
	return __hole

var __gui = null
func get_gui():
	if __gui == null:
		__gui = get_node("GUIs")
	return __gui

var __gui_pre_game = null
func get_pre_game_gui():
	if __gui_pre_game == null:
		__gui_pre_game = get_gui().get_node("PreGameGUI")
	return __gui_pre_game

var __gui_in_game = null
func get_in_game_gui():
	if __gui_in_game == null:
		__gui_in_game = get_gui().get_node("InGameGUI")
	return __gui_in_game

var __gui_end_game = null
func get_end_game_gui():
	if __gui_end_game == null:
		__gui_end_game = get_gui().get_node("EndGameGUI")
	return __gui_end_game

var __gui_hold_mouse = null
func get_hold_mouse():
	if __gui_hold_mouse == null:
		__gui_hold_mouse = get_pre_game_gui().get_node("HoldMouse")
	return __gui_hold_mouse

var __gui_release_mouse = null
func get_release_mouse():
	if __gui_release_mouse == null:
		__gui_release_mouse = get_pre_game_gui().get_node("ReleaseMouse")
	return __gui_release_mouse

var __gui_power_indicator = null
func get_power_indicator():
	if __gui_power_indicator == null:
		__gui_power_indicator = get_pre_game_gui().get_node("PowerIndicator")
	return __gui_power_indicator

var __gui_in_game_info = null
func get_in_game_info():
	if __gui_in_game_info == null:
		__gui_in_game_info = get_in_game_gui().get_node("InGameInfo")
	return __gui_in_game_info

var __gui_end_game_info = null
func get_end_game_info():
	if __gui_end_game_info == null:
		__gui_end_game_info = get_end_game_gui().get_node("EndGameInfo")
	return __gui_end_game_info

var __gui_restart_button = null
func get_restart_button():
	if __gui_restart_button == null:
		__gui_restart_button = get_end_game_gui().get_node("Button")
	return __gui_restart_button

var __gui_touch = null
func get_ui_touch():
	if __gui_touch == null:
		__gui_touch = get_gui().get_node("TouchGUI")
	return __gui_touch

func display_end_game():
	if game_won == true:
		get_end_game_info().set_text(\
			"Congratulations!" + "\n" + \
			"Your score was: " + str(get_ball_body().get_distance())\
		)
	else:
		get_end_game_info().set_text(\
			"You lost!" + "\n" + \
			"Try again ... "\
		)

### Callbacks
func _on_StateTimer_timeout():
	state_timer.stop()
	state_can_change = true

func _on_BallTimer_timeout():
	ball_timer.stop()
	ball_timer_started = false
	game_won = false
	display_end_game()
	set_state(STATE.END)

func _on_Button_pressed():
	get_restart_button().set_focus_mode(Control.FOCUS_NONE)
	get_tree().call_group(0, "game_entity", "game_restart")
	set_state(STATE.EXPLORE)

func update_in_game_text():
	get_in_game_info().set_text(\
		"Start Velocity: " + str(get_ball_body().curr_power) + "\n" + \
		"Linear ball velocity: " + (str(get_ball_body().get_linear_velocity().length())) + "\n" + \
		"Distance: " + str(get_ball_body().get_distance())\
	)