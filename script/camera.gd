
extends KinematicBody

const MODE_FREE = 0
const MODE_FOLLOW = 1

const CAM_MOVE_FOR = "camera_move_forward"
const CAM_MOVE_BACK = "camera_move_backward"
const CAM_MOVE_LEFT = "camera_move_left"
const CAM_MOVE_RIGHT = "camera_move_right"
const CAM_MOVE_UP = "camera_move_up"
const CAM_MOVE_DOWN = "camera_move_down"

export var camera_speed = 10.0;
export var view_sensitivity = 0.3

var STATE = preload("states.gd")

var mode = MODE_FREE

var moved = false
var motion = null
var pos = Vector3(0,0,0)
var yaw = 0;
var pitch = 0;
var center = 0;
var aim = null
var strafe = null
var camera = null

func _ready():
	add_to_group("game_entity")
	camera = get_node("Camera")
	set_collide_with_rigid_bodies(false)
	center = get_viewport().get_visible_rect().size / 2
	go_to_spot()

func parse_input(ev):
	if ev.type == InputEvent.MOUSE_MOTION:
		var sense = view_sensitivity
		if ev.type == InputEvent.SCREEN_DRAG:
			sense /= 2
		yaw = fmod(yaw - ev.relative_x * sense, 360)
		pitch = max(min(pitch - ev.relative_y * sense, 90), -90)
		set_rotation(Vector3(0, deg2rad(yaw), 0))
		camera.set_rotation(Vector3(deg2rad(pitch), 0, 0))

func parse_keyboard(delta):
	# Get vector out of the camera
	var aim = camera.project_ray_normal(center) * delta * camera_speed
	# Get the vector for strafing
	var strafe = get_global_transform().basis[2] * delta * camera_speed
	if Input.is_action_pressed(CAM_MOVE_FOR):
		pos += aim
		moved = true
	if Input.is_action_pressed(CAM_MOVE_BACK):
		pos -= aim
		moved = true
	if Input.is_action_pressed(CAM_MOVE_LEFT):
		pos += Vector3(-strafe.z, 0, strafe.x)
		moved = true
	if Input.is_action_pressed(CAM_MOVE_RIGHT):
		pos -= Vector3(-strafe.z, 0, strafe.x)
		moved = true
	if Input.is_action_pressed(CAM_MOVE_UP):
		pos += Vector3(0, camera_speed * delta , 0)
		moved = true
	if Input.is_action_pressed(CAM_MOVE_DOWN):
		pos += Vector3(0, -camera_speed * delta , 0)
		moved = true
	# Move the camera, if is colliding, bring it back to an allowed position
	if moved:
		moved = false
		motion = move(pos)
		if is_colliding():
			var n = get_collision_normal()
			motion = n.slide( motion )
			move(motion)
		pos = Vector3(0,0,0)

func go_to_spot():
	var ball = get_node("../CornerSpot")
	var ball_camera = ball.get_node("CornerPitch")
	set_translation(ball_camera.get_global_transform().origin)
	yaw = rad2deg(ball.get_rotation().y)
	pitch = rad2deg(ball_camera.get_rotation().x)
	set_rotation(Vector3(0, deg2rad(yaw), 0))
	camera.set_rotation(Vector3(deg2rad(pitch), 0, 0))
	camera.make_current()

func state_changed(state):
	if state == STATE.EXPLORE or state == STATE.PLAY:
		show()
		go_to_spot()
	else:
		hide()